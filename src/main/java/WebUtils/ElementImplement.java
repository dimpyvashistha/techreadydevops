package WebUtils;

import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class ElementImplement{

    webWaitUtils webWait = new webWaitUtils();
    
    /**
     * Click on the Mobile Element
     * @author dimpyv
     * @param driver
     * @param timeout
     * @param element
     */
    public void click(WebDriver driver, int timeout, WebElement element) {
        webWait.explicitWait(driver, timeout, element);
        String script = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);\n" +
                "var elementTop = arguments[0].getBoundingClientRect().top;\n" +
                "window.scrollBy(0, elementTop-(viewPortHeight/2));";
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript(script, element);
        element.click();
      }

    /**
     * Write Text in the Mobile Text box
     * @author dimpyv
     * @param driver
     * @param timeout
     * @param element
     * @param text
     */
    public void writeText(WebDriver driver, int timeout, WebElement element, CharSequence... text) {
        webWait.explicitWait(driver, timeout, element);
        element.clear();
        element.sendKeys(text);
    }

    /**
     * Select the value from Dropdown through 
     * @author dimpyv    
     * @param xpath
     * @param text
     * @throws InterruptedException 
     */
    public void selectValueFromDropdown(WebDriver driver, String xpath, String text) throws InterruptedException{
        webWait.implicitWait(driver, 60);
        List<WebElement> listElements = driver.findElements(By.xpath(xpath));
        if(!listElements.contains(text)){
            Thread.sleep(3000);
        }
        listElements = driver.findElements(By.xpath(xpath));
        if(listElements.size() > 0){
            for(WebElement ele : listElements){
                if(ele.getText().equals(text)){
                    ele.click();
                    break;
                }
            }
        }else{
            //LogResults.logStep().log(LogStatus.FAIL, "Elements size is Zero", String.valueOf(listElements.size()));
        }
    }

    public void navigateToURL(WebDriver driver, String url){
        driver.navigate().to(url);
    }
}

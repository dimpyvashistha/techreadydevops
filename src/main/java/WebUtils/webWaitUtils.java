package WebUtils;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class webWaitUtils implements waitInterface{

    
    public void implicitWait(WebDriver driver, int timeout){
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.MILLISECONDS);
    }
    
    public void explicitWait(WebDriver driver, int timeout, WebElement element){
        new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));
    }
    
    @SuppressWarnings({"unchecked", "rawtypes", "deprecation"})
    public void fluentWait(WebDriver driver, int timeout, WebElement element){
        new FluentWait(driver)
        .withTimeout(timeout, TimeUnit.SECONDS)
        .pollingEvery(250, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .ignoring(TimeoutException.class)        
        .until(ExpectedConditions.visibilityOf(element));
    }
}

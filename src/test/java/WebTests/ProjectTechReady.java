package WebTests;

import WebPages.DashboardPage;
import WebPages.HomePage;
import WebPages.LoginPage;
import WebPages.SignUpPage;
import WebUtils.ElementImplement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class ProjectTechReady extends BaseClass {

    ElementImplement ele = new ElementImplement();
    String firstName = "Tester_FirtName";
    String lastName = "Tester_LasName";
    String email = "POC_TechReady" + java.time.LocalDateTime.now().toString().replace(":", "_").replace(".", "_").replace("-", "_") + "@mailinator.com";
    String password = "Password1";
    String phone = "1234567890";


    /**
     * Test the Sign Up Flow
     *
     * @author dimpyv
     */
    @Test
    public void testSignUp() {
        //Navigate to the URL
        ele.navigateToURL(driver, "https://www.phptravels.net/");

        //Click the Sign Up Button
        HomePage homePage = new HomePage(driver);
        homePage.clickSignUpButton();

        //Click Got It cookies button
        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.clickGotItCookiesButton();

        //Enter the Sign Up details
        SignUpPage signUp = new SignUpPage(driver);
        signUp.enterSignUpDetails(firstName, lastName, phone, email, password);

        //Printing the Generated Email
        System.out.println(email);
    }

    /**
     * Test the Successful login Scenario
     *
     * @author dimpyv
     */
    @Test
    public void testLogin() {
        //Navigate to the URL
        ele.navigateToURL(driver, "https://www.phptravels.net/");

        //Click Login Button
        HomePage homePage = new HomePage(driver);
        homePage.clickLoginButton();

        //Click Got It cookies button
        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.clickGotItCookiesButton();

        //Enter Email and Password for login
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterUserNamePassword("POC_TechReady2021_09_20T13_30_11_665@mailinator.com", "Password1");

        //Click on Login button
        loginPage.clickLoginButton();

        //Asserting the First Name displayed on the Dashboard
        DashboardPage dashboardPage = new DashboardPage(driver);
        Assert.assertEquals(dashboardPage.getFirstNameFromDashboard(), "Tester_FirtName");
    }


    /**
     * Tear Down method
     *
     * @author dimpyv
     */
    @AfterMethod
    public void teardown() {
        driver.close();
    }
}

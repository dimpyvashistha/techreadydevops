package WebTests;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.util.HashMap;

public class BaseClass {

    public static WebDriver driver ;
    WebDriver currentDriver;
    String driverPath =  "./src/test/resources/drivers/";
    String extension = ".exe";

    @BeforeMethod(alwaysRun = true)
    public void setup() throws Exception {
        System.setProperty("webdriver.chrome.driver", new File(driverPath + "chromedriver" + extension).getCanonicalPath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        options.addArguments("--disable-notifications");
        HashMap<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.media_stream_mic", 1);
        options.setExperimentalOption("prefs", prefs);
        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        //To avoid NoAlerTException by changing default behaviour of the driver
        options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
        currentDriver = new ChromeDriver(options);
        currentDriver.manage().window().setSize(new Dimension(1024, 768));
        currentDriver.manage().window().maximize();
        currentDriver.manage().deleteAllCookies();
        driver = currentDriver;
    }
}
package WebPages;

import WebUtils.ElementImplement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage {

    private WebDriver driver;
    ElementImplement webElementOperations =  new ElementImplement();

    /**
     * Declaring Constructor
     *
     * @param driver
     */
    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Click on Sign Up Button
     *
     * @author dimpyv
     */
    public void clickSignUpButton(){
        webElementOperations.click(driver,1000,btnSignUp);
    }

    /**
     * Click on Login Button
     *
     * @author dimpyv
     */
    public void clickLoginButton(){
        webElementOperations.click(driver,1000,btnLogin);
    }
    
    @FindBy(xpath = "//a[@class='theme-btn theme-btn-small waves-effect']")
    WebElement btnSignUp;
    @FindBy(xpath = "//a[@class='theme-btn theme-btn-small theme-btn-transparent ml-1 waves-effect']")
    WebElement btnLogin;
}

package WebPages;

import WebUtils.ElementImplement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class SignUpPage {

    private WebDriver driver;
    ElementImplement webElementOperations =  new ElementImplement();

    /**
     * Declaring Constructor
     *
     * @param driver
     */
    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Click on Sign Up Button
     *
     * @author dimpyv
     */
    public void enterSignUpDetails(String firstName, String lastName, String phone, String email, String password ){
        webElementOperations.writeText(driver,1000,txtFirstName,firstName);
        webElementOperations.writeText(driver,1000,txtLastName,lastName);
        webElementOperations.writeText(driver,1000,txtPhone,phone);
        webElementOperations.writeText(driver,1000,txtEmail,email);
        webElementOperations.writeText(driver,1000,txtPassword,password);
        webElementOperations.click(driver,1000,btnSignUp);
    }

    /**
     * Click Got It Cookies Button
     *
     * @author dimpyv
     */
    public  void clickGotItCookiesButton(){
        webElementOperations.click(driver,1000,btnCookiesOk);
    }

    //Elements
    @FindBy(xpath = "//input[@name='first_name']")
    WebElement txtFirstName;
    @FindBy(xpath = "//input[@name='last_name']")
    WebElement txtLastName;
    @FindBy(xpath = "//input[@name='phone']")
    WebElement txtPhone;
    @FindBy(xpath = "//input[@name='email']")
    WebElement txtEmail;
    @FindBy(xpath = "//input[@name='password']")
    WebElement txtPassword;
    @FindBy(xpath = "//span[@class='ladda-label' and text()='Signup']")
    WebElement btnSignUp;
    @FindBy (xpath = "//button[@id='cookie_stop']")
    WebElement btnCookiesOk;
}
